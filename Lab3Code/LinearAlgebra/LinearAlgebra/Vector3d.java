//Santiago Luna 2032367

package LinearAlgebra;
import java.math.*;

public class Vector3d{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    } 

    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        double magnitude;

        magnitude = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
        return magnitude;
    }

    public double dotProduct(Vector3d v){
        double productX, productY, productZ, dotProduct;

        productX = this.x * v.getX();
        productY = this.y * v.getY();
        productZ = this.z * v.getZ();
        dotProduct = productX + productY + productZ;

        return dotProduct;
    }

    public Vector3d add(Vector3d v){
        double sumX, sumY, sumZ;

        sumX = this.x + v.getX();
        sumY = this.y + v.getY();
        sumZ = this.z + v.getZ();

        Vector3d vSum = new Vector3d(sumX, sumY, sumZ);
        return vSum;
    }

}